create or replace view hubspot.companies_v as (
select h.organization_id, h.hs_object_id, h.organization_name,
       coalesce(tm.label, 'Other') as type,
       av.label  as account_value,
       h.registration_date,
       coalesce(cm.corrected_country, h.country) as country,
    case
       when coalesce(cso.first_name, cso.last_name) is null then cso.email
       else trim(coalesce(cso.first_name, '') || ' ' || coalesce(cso.last_name, ''))
    end                                    as cs_owner,
    case
       when coalesce(co.first_name, co.last_name) is null then co.email
       else trim(coalesce(co.first_name, '') || ' ' || coalesce(co.last_name, ''))
    end                                    as owner,
    h.subscription_status,
    h.annualrevenue as annual_revenue,
    CASE
        WHEN COALESCE(tm.label, 'Other') = 'End-Customer' AND COALESCE(av.label, 'Empty') IN ('Corporate', 'SMB', 'Enterprise') THEN true
        ELSE false
    END as is_mql
from hubspot.companies h
left join dictionaries.account_values av on  h.ec_verification_type = av.hs_internal_value
left join dictionaries.hs_company_type_mapping_lite tm
    on tm.internal_value = coalesce(h.company_type__manual_, h.company_type)
left join country_mapping cm on cm.country = trim(regexp_replace(h.country, '	', ''))
LEFT JOIN hubspot.owners cso ON h.cs_owner = cso.id
LEFT JOIN hubspot.owners co ON h.hubspot_owner_id = co.id
                                              );
-------------------------------------------------------------------------------
create materialized view presentation.organizations_mv as
with correct_companies as (select *
                           from hubspot.companies_v
                           where organization_id in (select organization_id
                                                     from hubspot.companies
                                                     group by organization_id
                                                     having count(*) = 1)
                           )
select
    p.organization_id
    , p.organization_name,
    coalesce(c.type, c2.type) as type,
    coalesce(c.account_value, c2.account_value, fa.ec_verification_type) as account_value,
    case when p.created_at_dttm::date >= '2022-05-25' then p.created_at_dttm
        else fa.registration_date
    end as registration_date,
    coalesce(c.registration_date, c2.registration_date) as registration_date_from_hs,
    coalesce(c.country, c2.country, fa.country) as region,
    coalesce(c.subscription_status, c2.subscription_status) = 'Subscribed' as subscription_is_active,
    coalesce(c.owner, c2.owner) as owner,
    coalesce(c.cs_owner, c2.cs_owner )as cs_owner,
    coalesce(c.hs_object_id, c2.hs_object_id) as crm_id,

    fa.dirty_type as dirty_type,
    case when coalesce(ms.corporate_email, 0) = 0 then 'Personal' else 'Corporate' end as email_type,
    fa.account_id as first_account_id,
    fa.type as first_account_type_from_hs
    , CASE
        WHEN fa.dirty_type = 'End-Customer' AND (fa.type not in ('LSP', 'Education', 'Test', 'Fraud')) OR
             COALESCE(fa.type, fa.dirty_type) = 'End-Customer' THEN 'End-Customer'
        WHEN fa.type = 'Personal' OR COALESCE(fa.type, fa.dirty_type) = 'Freelancer' THEN 'Freelancer'
        WHEN fa.type not in ('LSP', 'Education', 'Test', 'Fraud') THEN 'Other'
        WHEN COALESCE(fa.type, fa.dirty_type) = 'Education' THEN 'Education'
        WHEN COALESCE(fa.type, fa.dirty_type) = 'LSP' THEN 'LSP'
        ELSE 'Other'
    END AS first_account_type_based_on_hs_type_and_product_dirty_type,
    c.organization_id  flag_id,
    coalesce(c.annual_revenue, c2.annual_revenue) as annual_revenu,
    coalesce(c.organization_name, c2.organization_name)  as hs_company_name,
    coalesce(c.is_mql, c2.is_mql)  as is_mql
from smartcat_prod.organizations_names p
left join correct_companies c on p.organization_id = c.organization_id
left join dev.organization_parent_mapping opm on opm.organization_id = p.organization_id
left join accounts fa on fa.account_id = opm.parent_id
left join hubspot.companies_v c2 on c2.hs_object_id = fa.crm_id
left join pql_score ms on ms.account_id = fa.account_id
;
alter materialized view presentation.organizations_mv owner to bi_reader;
alter table presentation.organizations add column is_mql text;
select * from presentation.organizations_mv limit 10;
---------------------------------------------------------------------------------------


select i.assignee, i.requester,
    count(*) filter (where dev.interval_no_holy(greatest(a_from, i_from), least(a_till, i_till, now()::timestamp)) > 24)/
       count(*) * 100
from presentation.youtrack_support_stats s
join youtrack.issues i on i.full_id = s.full_id
group by i.assignee, i.requester;
create function get_marketing_channel(traffic_medium character varying, utm_medium character varying, traffic_campaign character varying, utm_campaign character varying, traffic_source character varying, utm_source character varying, lp_sign character varying, product_sign character varying, landing_page character varying, OUT sc_channel character varying) returns character varying
    immutable
    language sql
as
$$
select case when (landing_page ilike 'https://smartcat.com%' or landing_page ilike 'https://us.smartcat.com%' or landing_page ilike 'https://ea.smartcat.com%') then 'product:' || coalesce (product_sign, '')
            when traffic_medium = 'email' then 'email:' || coalesce (traffic_source, utm_source, '') || ':' || coalesce (traffic_campaign, utm_campaign, '')
            when traffic_medium = 'cpc' or utm_medium = 'cpc' then 'cpc:' || coalesce (traffic_source, utm_source, '') || ':' || coalesce (traffic_campaign, utm_campaign, '')
            when traffic_medium in ('Linkedin', 'linkedin', 'social') then 'social:' || coalesce (traffic_source, utm_source, '') || ':' || coalesce (traffic_campaign, utm_campaign, '')
            when lp_sign = 'blog' then 'blog:' || coalesce (substring (landing_page, '.*blog\/(.*)\/'), '')
            when lp_sign = 'marketplace' then substring (landing_page, '(marketplace\/user|marketplace\/*(?!user).*)')
            when traffic_campaign ilike 'freelancer-referral%' then 'freelancer-referral'
            when (product_sign is not null and product_sign != '') then 'product:' || coalesce (product_sign, '')
            when (lp_sign is not null and lp_sign != '') then 'website:' || coalesce (lp_sign, '')
            when product_sign = 'referral' then 'direct:referral'

            else 'website:'
        end as sc_channel
$$;

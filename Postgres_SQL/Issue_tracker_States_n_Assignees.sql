create materialized view presentation.youtrack_support_stats as
WITH assignee_period AS (SELECT ah.full_id,
                                ah.new_state                                                 AS assignee,
                                ah.changed_at                                                AS "from",
                                COALESCE(lead(ah.changed_at) OVER (PARTITION BY ah.full_id ORDER BY ah.changed_at),
                                         '2075-01-01 00:00:00'::timestamp without time zone) AS till
                         FROM youtrack.assignee_hist ah
                                  LEFT JOIN youtrack.issues i_1 ON ah.full_id::text = i_1.full_id::text),
     state_period AS (SELECT iss.full_id,
                             iss.new_state                                                AS state,
                             iss.set_at                                                   AS "from",
                             COALESCE(lead(iss.set_at) OVER (PARTITION BY iss.full_id ORDER BY iss.set_at),
                                      '2075-01-01 00:00:00'::timestamp without time zone) AS till
                      FROM youtrack.issue_states iss
                               LEFT JOIN youtrack.issues i_1 ON iss.full_id = i_1.full_id::text)
SELECT a.full_id,
       a.assignee,
       a."from" AS a_from,
       a.till   AS a_till,
       i.state,
       i."from" AS i_from,
       i.till   AS i_till
FROM assignee_period a
         LEFT JOIN state_period i ON a.full_id::text = i.full_id AND (i."from" >= a."from" AND i."from" <= a.till OR
                                                                      a."from" >= i."from" AND a."from" <= i.till) AND
                                     i."from" <> i.till
WHERE split_part(a.full_id::text, '-'::text, 1) = 'SUPPORT'::text;
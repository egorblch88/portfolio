create function interval_no_holy(start timestamp, finish timestamp, OUT result numeric(10,2)) returns character varying
    immutable
    language plpgsql
as
$$
result := 12.2
$$;

create or replace function dev.interval_no_holy(start timestamp, finish timestamp) returns numeric(10,6)
as $$
    declare
    res numeric(10,6);
        BEGIN
            res := 0;
            while true loop
                   if start::date = finish::date then res := res + (extract(epoch from finish - start)/3600)::numeric(10,2);
                   exit;
                   else
                        if EXTRACT(isodow FROM start::date) not in (6, 7)
                            then res := (extract(epoch from start::date + 1 - start)/3600)::numeric(10,2);
                        end if;
                        start := (start::date + 1)::timestamp;
                   end if;
            end loop;
            return res;
        END
$$ LANGUAGE plpgsql;

select dev.interval_no_holy('2022-11-28 23:59:00'::timestamp, '2022-11-30 23:00:00'::timestamp);
grant execute on function dev.interval_no_holy(timestamp, timestamp) to grafana;

select now()::date + 1 - now();
select EXTRACT(isodow FROM now()::date)
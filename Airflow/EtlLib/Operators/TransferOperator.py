from airflow.models import BaseOperator
import os
import threading
from airflow.utils.decorators import apply_defaults


class TransferOperator(BaseOperator):
    @apply_defaults
    def __init__(
            self,
            provider,
            consumers,
            *args,
            **kwargs,
    ) -> None:
        """

        :param provider:
        :param consumers:
        :param args:
        :param kwargs:
        """
        super().__init__(*args, **kwargs)
        self._provider = provider
        self._consumers = consumers

    def execute(self, context):
        provider_pipes = []
        consumer_threads = []
        for c in self._consumers:
            r_fd, w_fd = os.pipe()
            provider_pipes.append(w_fd)
            c_th = threading.Thread(
                target=c.run,
                args=(
                    r_fd,
                )
            )
            c_th.start()
            consumer_threads.append(c_th)
        self._provider.run(provider_pipes)
        for c_th in consumer_threads:
            c_th.join()
        for c in self._consumers:
            if c.exception:
                raise c.exception

-- $__dashboard - Dynamic by channel, graph - ${__user.login}
with united_analytics as (
    select distinct on (email) *
    from
        (select * , null::text as alt_registration_page from google.ga4_pseudo_analytics t
        where not exists(select 1 from google.ga4_pseudo_analytics_app where email = t.email)
        union all
        select * from google.ga4_pseudo_analytics_app) t1
),

hs as (

select *
from hubspot_deals
where pipeline = 'Sales Pipeline'
and name not like '%PQL%'
and stage != 'PQL qualified'
and subscriptions not like '%Forever Free%'

),

    duplicate_accounts AS
(SELECT DISTINCT
    accounts_original.account_id,
    accounts_original.account_name,
    COALESCE(accounts_duplicate.duplicate_id, accounts_original.account_id) as duplicate_id,
    COALESCE(accounts_duplicate.duplicate_name, accounts_original.account_name) as duplicate_name,
    COALESCE(accounts_duplicate.country, accounts_original.country) as duplicate_country,
    COALESCE(accounts_duplicate.ec_verification_type, accounts_original.ec_verification_type) as duplicate_verification,
    account_types.account_type as duplicate_type

FROM accounts as accounts_original
    LEFT JOIN accounts as accounts_duplicate
        ON COALESCE(accounts_original.duplicate_id, accounts_original.account_id) = COALESCE(accounts_duplicate.account_id) --, accounts_original.account_id)
    LEFT JOIN account_types
        ON COALESCE(accounts_duplicate.duplicate_id, accounts_original.account_id) = account_types.account_id
),

accs_with_ga as (
  select
          a.account_name,
          a.account_id,

          duplicate_accounts.duplicate_id,
          corrected_country as duplicate_country,
          duplicate_accounts.duplicate_type,

          registration_date,
          traffic_medium,
          utm_medium,
          utm_campaign,
          traffic_campaign,
          traffic_source,
          utm_source,
          landing_page,
          page_referrer,


          case when traffic_medium in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(traffic_medium, '\s', '_', 'g') end as traffic_medium_clear,
          case when utm_medium in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(utm_medium, '\s', '_', 'g') end as utm_medium_clear,
          case when utm_campaign in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(utm_campaign, '\s', '_', 'g') end as utm_campaign_clear,
          case when traffic_campaign in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(traffic_campaign, '\s', '_', 'g') end as traffic_campaign_clear,
          case when traffic_source in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(traffic_source, '\s', '_', 'g') end as traffic_source_clear,
          case when utm_source in ('','(direct)','(organic)','(referral)') then null
               else regexp_replace(utm_source, '\s', '_', 'g') end as utm_source_clear,


          case when duplicate_verification is null then 'None' else duplicate_verification end as duplicate_value,
          case when landing_page ~* '(https:\/\/)?[a-z]{0,3}\.?smartcat\.com\/\s*$' then 'index'
              else substring (landing_page, '.*(blog|marketplace|online-events|lp|register_popup|university|cat-tool|pricing|organizations|translation-management|unavailable|i-am-a-vendor|localization-process-management|career|autopilot|chat|integrations|extensions|translation-companies|for-e-learning-platforms|payments|privacy-policy|lsp|culture-code|community|about-us).*' )
         end as lp_sign,
         substring (landing_page, '.*(sign-in|projects|registration|workspace|join-team|Workspace|referral-program).*') as product_sign,

         --- hubspot data
         company_id,
         pipeline,
         name,
         stage,
         subscriptions,
         create_date,
         hs.close_date,
         close_won_date,
         case when hs.close_date is not null and close_won_date is null then hs.close_date end as lose_date

  from united_analytics ga
      right join accounts a
          on ga.email = a.created_by_email
             and ga.registration_timestamp::date = a.registration_date
      left join hs
          on company_id = crm_id
      LEFT JOIN duplicate_accounts
          ON a.account_id = duplicate_accounts.account_id
      left join country_mapping
          on duplicate_accounts.duplicate_country = country_mapping.country


  where case when '$metric' = 'MQLs' then $__timeFilter(registration_date)
             when '$metric' = 'Open deals' then $__timeFilter(create_date)
             when ('$metric' = 'Lost deals'and close_won_date is null) then $__timeFilter(hs.close_date)
             when '$metric' = 'Won deals' then $__timeFilter(close_won_date) end

)

select --account_value as metric,
       substring (get_marketing_channel_2 (traffic_medium_clear, utm_medium_clear, traffic_campaign_clear,utm_campaign_clear,traffic_source_clear ,utm_source_clear,lp_sign,product_sign, landing_page), '([^:\/]*)' ) as metric, --sc_channel
       case when '$metric' = 'MQLs' then date_trunc ('$Breakdown', registration_date)
            when '$metric' = 'Open deals' then date_trunc ('$Breakdown', create_date)
            when '$metric' = 'Lost deals' then date_trunc ('$Breakdown', lose_date)
            when '$metric' = 'Won deals' then date_trunc ('$Breakdown', close_won_date) end as time,
       --date_trunc ('$Breakdown', registration_date) as time,
       count (distinct account_id) as metric2
from accs_with_ga
where duplicate_value in ($Account_value)
and duplicate_type in ($Account_type)
and duplicate_country in ($country)
and substring (get_marketing_channel_2 (traffic_medium_clear, utm_medium_clear, traffic_campaign_clear,utm_campaign_clear,traffic_source_clear ,utm_source_clear,lp_sign,product_sign, landing_page), '([^:\/]*)' ) in ($channel)
group by 1,2
order by 2

CREATE DATABASE IF NOT EXISTS gaia ON CLUSTER 'ch-dwh-l1';

select currentUser();
select currentDatabase();
SELECT * FROM system.replicas;
select * from system.users;
SHOW roles;
select * from system.databases;
show databases;
show access;
--------------roles------------------------------------

create role if not exists admin on cluster 'ch-dwh-l1';
grant on cluster 'ch-dwh-l1' all on *.* to admin with grant option;
create user if not exists "v.egorov" on cluster 'ch-dwh-l1' identified with sha256_password by 'apos70ea';
grant on cluster 'ch-dwh-l1' admin to "v.egorov";

create role if not exists  gaia_user;
grant select on gaia.* to gaia_user;

create user if not exists "a.gubkin" identified with sha256_password by 'macD4W1a';
grant gaia_user to "a.gubkin";

create role if not exists gaia_executor on cluster 'ch-dwh-l1';
grant select, truncate, insert, alter on gaia.* to gaia_executor on cluster 'ch-dwh-l1';
create user s7tr_airflow identified with sha256_password by '12&jqq7e' on cluster 'ch-dwh-l1';
grant gaia_executor to s7tr_airflow on cluster 'ch-dwh-l1';
----------------------------------------------------------------------

CREATE TABLE if not exists gaia.event_search_local ON CLUSTER 'ch-dwh-l1'
(
    id                Int64,
    user_id           Nullable(String),
    operation         Nullable(String),
    service_version   Nullable(String),
    request_type      Nullable(String),
    pnr               Nullable(String),
    digest            Nullable(String),
    conversation_id   Nullable(String),
    start_time        DateTime64(3, 'Europe/Moscow'),
    end_time          Nullable(DateTime64(3, 'Europe/Moscow')),
    spent_time        Nullable(Int32),
    supplier_error    Nullable(String),
    error_type        Nullable(String),
    error_message     Nullable(String),
    error_source      Nullable(String),
    partner_pnr       Nullable(String),
    use_cases         Nullable(String),
    request_size      Nullable(Int32),
    response_size     Nullable(Int32),
    file_log_location Nullable(String),
    value             Nullable(Int32)
) ENGINE = ReplicatedMergeTree('/clickhouse/tables/{layer}-{shard}/event_search_local', '{replica}')
    PARTITION BY (toYYYYMMDD(start_time)) ORDER BY (id)
    SETTINGS storage_policy = 'default', index_granularity = 8192;

CREATE TABLE gaia.event_search
(
    id                Int64,
    user_id           Nullable(String),
    operation         Nullable(String),
    service_version   Nullable(String),
    request_type      Nullable(String),
    pnr               Nullable(String),
    digest            Nullable(String),
    conversation_id   Nullable(String),
    start_time        DateTime64(3, 'Europe/Moscow'),
    end_time          Nullable(DateTime64(3, 'Europe/Moscow')),
    spent_time        Nullable(Int32),
    supplier_error    Nullable(String),
    error_type        Nullable(String),
    error_message     Nullable(String),
    error_source      Nullable(String),
    partner_pnr       Nullable(String),
    use_cases         Nullable(String),
    request_size      Nullable(Int32),
    response_size     Nullable(Int32),
    file_log_location Nullable(String),
    value             Nullable(Int32)
)ENGINE = Distributed('ch-dwh-l1', 'gaia', 'event_search_local', rand()) ;

CREATE TABLE if not exists gaia.event_search_deduplicated_local ON CLUSTER 'ch-dwh-l1'
(
    id                Int64,
    digest            Nullable(String),
    start_time        DateTime64(3, 'Europe/Moscow')
) ENGINE = MergeTree
    PARTITION BY (toYYYYMMDD(start_time)) ORDER BY (id)
    SETTINGS storage_policy = 'default', index_granularity = 8192;

CREATE TABLE gaia.event_search_deduplicated
(
    id                Int64,
    digest            Nullable(String),
    start_time        DateTime64(3, 'Europe/Moscow')
)ENGINE = Distributed('ch-dwh-l1', 'gaia', 'event_search_deduplicated_local', rand()) ;

CREATE TABLE if not exists gaia.event_search_parsed
(           search_date   Date,
--             digest      Nullable(String),
--             itinerary   Nullable(String),
            route_type       Nullable(String),
--             ticket_route  Nullable(String),
--             route_segment  Nullable(String),
            round_segment_code  Nullable(String),
            departure_date   Nullable(Date),
            iata_origin   Nullable(String),
            iata_destination  Nullable(String),
--             return_point_stay  Nullable(Int64),
--             segment_stay        Nullable(Int64),
            unique_searches     Nullable(Int64)

) ENGINE = MergeTree
    PARTITION BY (search_date) ORDER BY (search_date)
    SETTINGS storage_policy = 'default', index_granularity = 8192;

--drop table gaia.event_search_parsed;
create table if not exists gaia.airports
(
    iata_code      String,
    city_code       Nullable(String),
    city_name       Nullable(String),
    country_name    Nullable(String)
) ENGINE = MergeTree
    PARTITION BY (iata_code) ORDER BY (iata_code)
    SETTINGS storage_policy = 'default', index_granularity = 8192;

select * from gaia.event_search_window15tuple_set;
drop table gaia.event_search_window15tuple_set;

CREATE TABLE if not exists gaia.event_search_window15tuple_set
(           id                Nullable(Int64),
            digest            Nullable(String),
            start_time        Nullable(DateTime64(3, 'Europe/Moscow') )
) ENGINE = Set;
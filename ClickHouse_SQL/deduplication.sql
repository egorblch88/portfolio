insert into gaia.event_search_deduplicated
        select id,
               digest_json as digest,
               start_time
        from (
              select id,
                     digest,
                     start_time,
                     digest like '%%origin%%'                         as digest_format_version,
                     if(
                             digest_format_version, [''],
                             splitByChar(',', coalesce(digest, ''))
                         )                                            as segments_str,
                     arrayMap(x -> splitByChar('-', x), segments_str) as segments_arr,
                     if(
                             digest_format_version, [''],
                             arrayMap((x, y) -> '{"origin":"' || x[1] ||
                                                '","destination":"' || x[3] ||
                                                '","departure":"' ||
                                                substr(
                                                        toString(
                                                                if(
                                                                        match(x[2], '^(\\d{2}[A-Za-z]{3})(\\d{2})$'),
                                                                        parseDateTimeBestEffortOrZero(
                                                                                substr(x[2], 1, 5) || '20' || substr(x[2], 6, 2),
                                                                                'UTC'),
                                                                        parseDateTimeBestEffort('1970-01-01 00:00:00', 'UTC'))
                                                            ), 1, 10
                                                    ) ||
                                                '","ordinalNumber":' || toString(y) ||
                                                '}',
                                      segments_arr,
                                      arrayEnumerate(segments_str))
                         )                                            as segments_json_rough,
                     if(
                             digest_format_version, digest,
                             '[' || arrayStringConcat(segments_json_rough, ',') || ']'
                         )                                            as digest_json
              from (
                    select ids[1]         as id,
                           digest,
                           start_times[1] as start_time
                    from (
                          select window_15,
                                 digest,
                                 groupArray(start_time) as start_times,
                                 groupArray(id)         as ids
                          from (
                                select 30 * intDiv(toUnixTimestamp(start_time), 30) as window_15,
                                       digest,
                                       start_time,
                                       id
                                from gaia.event_search es
                                where request_type = 'NDC'
                                  and digest is not null
                                  and digest <> 'null'
                                  and digest <> '[]'
                                  and coalesce(error_type, '') not like '%%validat%%'
                                  and toDate(start_time) = '%s'
                                order by window_15, digest, start_time, id
                                   )
                          group by window_15, digest
                             )
                       )
                 )
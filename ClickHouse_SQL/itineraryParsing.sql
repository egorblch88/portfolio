insert into gaia.event_search_parsed
        select search_date,
               route_type,
               round_segment_code,
               departure_date,
               iata_origin,
               iata_destination,
               count(*) as unique_searches
        from (
              select range(JSONLength(coalesce(digest, '')))                                       as array_indices,
                     arrayMap(x -> JSONExtractString(digest, x + 1, 'origin'), array_indices)      as origins,
                     arrayMap(x -> JSONExtractString(digest, x + 1, 'destination'), array_indices) as destinations,
                     arrayMap(
                             x -> coalesce(
                                     JSONExtractString(digest, x + 1, 'departure'),
                                     '1970-01-01 00:00:00'), array_indices)                        as departures,
                     arrayMap(x -> JSONExtractInt(digest, x + 1, 'ordinalNumber'), array_indices)  as ordinal_numbers,
                     arrayMap(x -> if(match(x, '^[а-яА-ЯёЁ\\w]{3}$'), x, null),
                              origins)                                                             as departure_airports_arr,
                     arrayMap(x -> if(match(x, '^[а-яА-ЯёЁ\\w]{3}$'), x, null),
                              destinations)                                                        as arrival_airports_arr,
                     departure_airports_arr[1]                                                     as first_departure_airport,
                     arrival_airports_arr[segments_count]                                          as last_arrival_airport,
                     multiIf(
                             first_departure_airport = 'LHR', 'LON',
                             first_departure_airport = 'PEK', 'BJS',
                             first_departure_airport = 'BKA', 'MOW',
                             first_departure_airport = 'CKL', 'MOW',
                             first_departure_airport = 'DME', 'MOW',
                             first_departure_airport = 'SVO', 'MOW',
                             first_departure_airport = 'VKO', 'MOW',
                             first_departure_airport = 'RVH', 'LED',
                             first_departure_airport = 'CDG', 'PAR',
                             first_departure_airport = 'HND', 'TYO',
                             first_departure_airport)                                              as first_departure_city,
                     multiIf(
                             last_arrival_airport = 'LHR', 'LON',
                             last_arrival_airport = 'PEK', 'BJS',
                             last_arrival_airport = 'BKA', 'MOW',
                             last_arrival_airport = 'CKL', 'MOW',
                             last_arrival_airport = 'DME', 'MOW',
                             last_arrival_airport = 'SVO', 'MOW',
                             last_arrival_airport = 'VKO', 'MOW',
                             last_arrival_airport = 'RVH', 'LED',
                             last_arrival_airport = 'CDG', 'PAR',
                             last_arrival_airport = 'HND', 'TYO',
                             last_arrival_airport)                                                 as last_arrival_city,
                     length(departure_airports_arr)                                                as segments_count,
                     segments_count / 2 = intDiv(segments_count, 2)                                as even_segments_count,
                     arrayConcat(departure_airports_arr,
                                 [arrival_airports_arr[segments_count]])                           as itinerary_departures,
                     arrayConcat([departure_airports_arr[1]], arrival_airports_arr)                as itinerary_arrivals,
                     arrayMap(
                             (x, y) -> coalesce(if(x = y, x, x || ';' || y), 'NULL'),
                             itinerary_departures,
                             itinerary_arrivals)                                                   as itinerary_by_airports_rough,
                     if(
                             segments_count > 1,
                             arrayMap(
                                     (x, y) -> if(
                                             y between 2 and segments_count,
                                             [
                                                 if(
                                                         match(itinerary_by_airports_rough[y - 1], ';'),
                                                         splitByChar(';', coalesce(itinerary_by_airports_rough[y - 1], ''))[2],
                                                         itinerary_by_airports_rough[y - 1]), x,
                                                 if(
                                                         match(itinerary_by_airports_rough[y + 1], ';'),
                                                         splitByChar(';', coalesce(itinerary_by_airports_rough[y + 1], ''))[1],
                                                         itinerary_by_airports_rough[y + 1])], ['', '', '']),
                                     itinerary_by_airports_rough,
                                     concat(ordinal_numbers, [toInt64(segments_count + 1)])),
                             [['', '', '']])                                                       as return_points_by_airports_rough,
                     arrayMap(
                             x ->
                                 [
                                     multiIf(
                                             x[1] = 'LHR', 'LON',
                                             x[1] = 'PEK', 'BJS',
                                             x[1] = 'BKA', 'MOW',
                                             x[1] = 'CKL', 'MOW',
                                             x[1] = 'DME', 'MOW',
                                             x[1] = 'SVO', 'MOW',
                                             x[1] = 'VKO', 'MOW',
                                             x[1] = 'RVH', 'LED',
                                             x[1] = 'CDG', 'PAR',
                                             x[1] = 'HND', 'TYO',
                                             x[1]),
                                     x[2],
                                     multiIf(
                                             x[3] = 'LHR', 'LON',
                                             x[3] = 'PEK', 'BJS',
                                             x[3] = 'BKA', 'MOW',
                                             x[3] = 'CKL', 'MOW',
                                             x[3] = 'DME', 'MOW',
                                             x[3] = 'SVO', 'MOW',
                                             x[3] = 'VKO', 'MOW',
                                             x[3] = 'RVH', 'LED',
                                             x[3] = 'CDG', 'PAR',
                                             x[3] = 'HND', 'TYO',
                                             x[3])
                                     ],
                             return_points_by_airports_rough)                                      as return_points_by_cities_rough,
                     concat(
                             [itinerary_by_airports_rough[1]],
                             arrayPopBack(
                                     arrayPopFront(
                                             arrayMap(
                                                     (x, y) -> if(y[1] != '' and y[1] = y[3], x[2], ''),
                                                     return_points_by_airports_rough,
                                                     return_points_by_cities_rough))),
                             [itinerary_by_airports_rough[segments_count + 1]]
                         )                                                                         as return_points_by_airports,
                     multiIf(
                             route_type = 'OW' and segments_count = 1,
                             itinerary_by_airports_rough,
                             arrayFilter(x -> match(coalesce(x, ''), '^[а-яА-ЯёЁ\\w]{3}(;[а-яА-ЯёЁ\\w]{3})?$'),
                                         return_points_by_airports))                               as ond_by_airports_rough,
                     arrayPopBack(
                             arrayMap(x -> if(
                                     match(coalesce(x, ''), '^[а-яА-ЯёЁ\\w]{3};[а-яА-ЯёЁ\\w]{3}$'),
                                     splitByChar(';', coalesce(x, ''))[1],
                                     x),
                                      ond_by_airports_rough))                                      as origin_arr,
                     arrayPopFront(
                             arrayMap(x -> if(
                                     match(coalesce(x, ''), '^[а-яА-ЯёЁ\\w]{3};[а-яА-ЯёЁ\\w]{3}$'),
                                     splitByChar(';', coalesce(x, ''))[2],
                                     x),
                                      ond_by_airports_rough))                                      as destination_arr,
                     arrayMap(
                             (x, y) -> coalesce(x, 'NULL') || '-' || coalesce(y, 'NULL'),
                             origin_arr,
                             destination_arr)                                                      as ond_by_airports,
                     arrayMap(x -> toUnixTimestamp(parseDateTime64BestEffortOrZero(x), 'UTC'),
                              departures)                                                          as departure_dates_unix,
                     arrayMap(x -> x / 86400,
                              arrayDifference(departure_dates_unix))                               as departure_dates_unix_diff,
                     indexOf(departure_dates_unix_diff,
                             arrayMax(departure_dates_unix_diff))                                  as max_departure_date_diff_idx,
                     departure_airports_arr[max_departure_date_diff_idx]                           as return_point_by_departure_dates,
                     if(
                             route_type = 'RT',
                             array(
                                         coalesce(departure_airports_arr[1], 'NULL') || '-' ||
                                         coalesce(return_point_by_departure_dates, 'NULL'),
                                         coalesce(return_point_by_departure_dates, 'NULL') || '-' ||
                                         coalesce(arrival_airports_arr[segments_count], 'NULL')),
                             [coalesce(departure_airports_arr[1], 'NULL') || '-' ||
                              coalesce(arrival_airports_arr[segments_count], 'NULL')])             as ond_by_departure_dates,
                     multiIf(
                             route_type = 'OW' and segments_count = 1, 1,
                             route_type = 'RT' and segments_count = 2, 2,
                             route_type = 'RT' and even_segments_count = 1, 3,
                             route_type = 'RT' and even_segments_count = 0, 4,
                             route_type = 'OW' and length(ond_by_airports_rough) > 2, 5,
                             route_type = 'OW' and return_point_stay > 1, 6,
                             route_type = 'OW' and length(ond_by_airports_rough) = 2 and
                             return_point_stay < 2, 7,
                             8)                                                                    as rule,
                     if(
                             rule in (4, 6) or (rule = 3 and length(ond_by_airports_rough) < 3),
                             ond_by_departure_dates,
                             ond_by_airports)                                                      as ond,
                     splitByChar('-', ond[1])                                                      as ond_forward_arr,
                     splitByChar('-', ond[2])                                                      as ond_backward_arr,
                     date_trunc('day', start_time)                                                 as search_date,
                     if(
                             first_departure_city = last_arrival_city, 'RT',
                             'OW')                                                                 as route_type,
                     multiIf(
                             rule in (1, 7), ['FORWARD'],
                             rule in (2, 3, 4), ['FORWARD', 'BACKWARD'],
                             rule in (5, 6), ['FORWARD', 'FORWARD'],
                             [])                                                                   as round_segment_code,
                     multiIf(
                             rule = 1, departures,
                             rule = 2, [departures[1], departures[2]],
                             rule in (3, 5),
                             [departures[1], departures[toInt8(segments_count / 2) + 1]],
                             rule in (4, 6),
                             [departures[1], departures[max_departure_date_diff_idx]],
                             rule = 7,
                             [departures[1]],
                             [])                                                                   as departure_date,
                     multiIf(
                             rule in (1, 7), [ond_forward_arr[1]],
                             rule in (2, 3, 4, 5), ond_forward_arr,
                             rule = 6, [itinerary_departures[1], return_point_by_departure_dates],
                             [])                                                                   as iata_origin,
                     multiIf(
                             rule in (1, 7), [ond_forward_arr[2]],
                             rule in (2, 3, 4, 5), ond_backward_arr,
                             rule = 6,
                             [return_point_by_departure_dates, itinerary_arrivals[segments_count + 1]],
                             [])                                                                   as iata_destination,
                     departure_dates_unix_diff[max_departure_date_diff_idx]                        as return_point_stay
              from gaia.event_search_deduplicated
              where rule < 8
                and FROM_UNIXTIME(arrayMin(departure_dates_unix)) >= date_trunc('day', start_time)
                and not (first_departure_city = last_arrival_city and segments_count = 1)
            )
            array join round_segment_code
           , departure_date
           , iata_origin
           , iata_destination
        group by search_date,
                 route_type,
                 round_segment_code,
                 departure_date,
                 iata_origin,
                 iata_destination